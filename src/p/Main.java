/**
 * SPDX-License-Identifier: Unlicense
 */
package p;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;

/**
 * @author txt
 * @version 0.0.1
 */
public class Main {
        private static final Logger log = Logger.getAnonymousLogger();
        
        /**
         * @param args
         */
        public static void main(String[] args) {
                States ret = States.geschlossen;
                try {
                        for (String line : Files.readAllLines(
                                        Paths.get("/proc/tty/driver/serial"),
                                        Charset.defaultCharset())) {
                                if (line.contains("RI")) {
                                        ret = States.offen;
                                        break;
                                }
                        }
                        
                        FileWriter fw = new FileWriter(new File(
                                        "/tmp/chch-status"));
                        fw.write(ret.toString());
                        fw.close();
                } catch (AccessDeniedException e) {
                        log.severe("Access to " + e.getFile() + " denied.");
                } catch (IOException e) {
                        log.severe(e.getMessage());
                }
        }
        
}
