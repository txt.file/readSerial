# readSerial

Java Programm aus dem Jahr 2014. Wertet eine serielle Konsole aus und schreibt die Auswertung in eine Datei. Habe ich geschrieben um den Öffnungsstatus der Tür des Chaostreff Chemnitz mit anderen Programmen einfacher weiter verarbeiten zu können.

Diese Software steht unter der [The Unlicense](https://unlicense.org/).
